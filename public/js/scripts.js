$(document).ready(function () {
    var $window = $(window),
        $root   = $('html, body'),
        $body   = $('body'),
        lastScrollPos = $window.scrollTop(),
        scrollAnimation = null;
    $window.on('scroll', function () {
        lastScrollPos = $window.scrollTop();
    });
    $window.on('hashchange', function () {
        var $target = $(window.location.hash);
        if ($target.length > 0) {
            $window.scrollTop(lastScrollPos);
            $root.on('scroll wheel DOMMouseScroll mousewheel keyup touchmove', function () {
                $root.stop();
            });
            $root.animate({
                scrollTop: $target.offset().top
            }, {
                duration: 300,
                queue:    false,
                complete: function () {
                    $root.off('scroll wheel DOMMouseScroll mousewheel keyup touchmove');
                }
            });
        }
    });
    var $video = $('#video');
    var navbar = function () {
        var threshold = $video.outerHeight(true),
            current   = $window.scrollTop();
        if (current >= threshold) {
            $body.addClass('fixed-nav');
        } else {
            $body.removeClass('fixed-nav');
        }
    };
    $window.on('scroll', navbar);
    navbar();
    $('a[href^="#"]').each(function (i, a) {
        var $a   = $(a),
            href = $a.attr('href');
        if (href !== '#') {
            $a.click(function (e) {
                e.preventDefault();
                if (href !== window.location.hash) {
                    window.history.pushState(null, null, href);
                }
                lastScrollPos = $window.scrollTop();
                $window.trigger('hashchange');
            });
        }
    });
    videojs.options.flash.swf = '/vendor/video.js/video-js.swf';
    // Vídeo Cesens
    var videoCesensStarted = false;
    videojs('video-cesens').on("play", function () {
        $video.find('.content').addClass('hidden');
        $video.find('.video').removeClass('hidden');
        if (!videoCesensStarted) {
            ga('send', {
                hitType:       'event',
                eventCategory: 'Vídeos',
                eventAction:   'Reproducciones',
                eventLabel:    'Animación'
            });
            videoCesensStarted = true;
        }
    });
    videojs('video-cesens').on("pause", function () {
        $video.find('.content').removeClass('hidden');
        $video.find('.video').addClass('hidden');
    });
    videojs('video-cesens').on("ended", function () {
        $video.find('.content').removeClass('hidden');
        $video.find('.video').addClass('hidden');
        ga('send', {
            hitType:       'event',
            eventCategory: 'Vídeos',
            eventAction:   'Finalizaciones',
            eventLabel:    'Animación'
        });
    });
    $video.find('.play-video a').click(function (e) {
        e.preventDefault();
        videojs('video-cesens').play();
    });
    // Video entrevistas
    var videoInterviewsStarted = false;
    videojs('video-interviews').on("play", function () {
        $('.interviews .video-js').removeClass('hidden');
        $('.interviews .poster').addClass('hidden');
        if (!videoInterviewsStarted) {
            ga('send', {
                hitType:       'event',
                eventCategory: 'Vídeos',
                eventAction:   'Reproducciones',
                eventLabel:    'Entrevistas'
            });
            videoInterviewsStarted = true;
        }
    });
    videojs('video-interviews').on("pause", function () {
        $('.interviews .video-js').addClass('hidden');
        $('.interviews .poster').removeClass('hidden');
    });
    videojs('video-interviews').on("ended", function () {
        $('.interviews .video-js').addClass('hidden');
        $('.interviews .poster').removeClass('hidden');
        ga('send', {
            hitType:       'event',
            eventCategory: 'Vídeos',
            eventAction:   'Finalizaciones',
            eventLabel:    'Entrevistas'
        });
    });
    $('.interviews .poster').click(function (e) {
        e.preventDefault();
        videojs('video-interviews').play();
    });
    var $form    = $('#contact-form'),
        $name    = $('#contact-form_name'),
        $email   = $('#contact-form_email'),
        $message = $('#contact-form_message'),
        $button  = $form.find('button'),
        $modal        = $('#contacto .modal'),
        $modalContent = $modal.find('.modal-content'),
        $modalTitle   = $modal.find('.modal-title'),
        $modalText    = $modal.find('.modal-body p'),
        form  = {
            busy: false,
            submit: function () {
                if (form.busy || !form.validate()) {
                    return;
                }
                form.loading();
                $.ajax({
                    url:    $form.attr('data-url'),
                    method: $form.attr('data-method'),
                    data: {
                        name:    $name.val(),
                        email:   $email.val(),
                        message: $message.val()
                    },
                    dataType: 'json',
                    success:  form.success,
                    error:    form.error,
                    complete: form.complete
                });
            },
            validate: function () {
                var name    = $name.val(),
                    email   = $email.val(),
                    message = $message.val(),
                    valid   = true;
                if (name === '') {
                    $name.parent('.form-group').addClass('has-error');
                    valid = false;
                } else {
                    $name.parent('.form-group').removeClass('has-error');
                }
                if (email === '' || !email.match(/^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i)) {
                    $email.parent('.form-group').addClass('has-error');
                    valid = false;
                } else {
                    $email.parent('.form-group').removeClass('has-error');
                }
                if (message === '') {
                    $message.parent('.form-group').addClass('has-error');
                    valid = false;
                } else {
                    $message.parent('.form-group').removeClass('has-error');
                }
                return valid;
            },
            loading: function () {
                form.busy = true;
                $name.attr('disabled', true);
                $name.addClass('disabled');
                $email.attr('disabled', true);
                $email.addClass('disabled');
                $message.attr('disabled', true);
                $message.addClass('disabled');
                $button.attr('disabled', true);
                $button.addClass('disabled');
                $button.text($button.attr('data-loading'));
            },
            success: function (response) {
                console.log(response);
                if (response.status === 'SUCCESS') {
                    $name.val('');
                    $email.val('');
                    $message.val('');
                    form.message('success');
                    ga('send', {
                        hitType:       'event',
                        eventCategory: 'Formularios',
                        eventAction:   'Envíos',
                        eventLabel:    'Contacto'
                    });
                } else {
                    form.error(response);
                }
            },
            error: function (response) {
                console.error(response);
                form.message('error');
            },
            complete: function () {
                form.busy = false;
                $name.attr('disabled', false);
                $name.removeClass('disabled');
                $email.attr('disabled', false);
                $email.removeClass('disabled');
                $message.attr('disabled', false);
                $message.removeClass('disabled');
                $button.attr('disabled', false);
                $button.removeClass('disabled');
                $button.text($button.attr('data-submit'));
            },
            message: function (type, title, text) {
                if (type === 'success') {
                    $modalContent.addClass('success');
                    title = $form.attr('data-success-title');
                    text  = $form.attr('data-success-text');
                } else {
                    $modalContent.removeClass('success');
                }
                if (type === 'error') {
                    $modalContent.addClass('error');
                    title = $form.attr('data-error-title');
                    text  = $form.attr('data-error-text');
                } else {
                    $modalContent.removeClass('error');
                }
                $modalTitle.text(title);
                $modalText.text(text);
                $modal.modal('show');
            }
        };
    $modal.modal({
        show: false
    });
    var $nav = $('#navigation'),
        navPadding = function () {
            var scrollDiv = document.createElement('div');
            scrollDiv.className = 'modal-scrollbar-measure';
            $body.append(scrollDiv);
            var scrollbarWidth = scrollDiv.offsetWidth - scrollDiv.clientWidth;
            $body[0].removeChild(scrollDiv);
            return scrollbarWidth
        };
    $modal.on('show.bs.modal', function () {
        $nav.css('padding-right', navPadding() + 'px');
    });
    $modal.on('hidden.bs.modal', function () {
        $nav.css('padding-right', 0);
    });
    $form.on('submit', function (e) {
        e.preventDefault();
        form.submit();
    });
    $name.keydown(function () {
        $(this).parent('.form-group').removeClass('has-error');
    });
    $email.keydown(function () {
        $(this).parent('.form-group').removeClass('has-error');
    });
    $message.keydown(function () {
        $(this).parent('.form-group').removeClass('has-error');
    });
    var $columnas  = $('.lists .row > div'),
        $cabeceras = $('.numbers .row.numbers .cell');
    $columnas.mouseover(function () {
        $($cabeceras[$columnas.index(this)]).addClass('hover');
    });
    $columnas.mouseout(function () {
        $cabeceras.removeClass('hover');
    });

    // sliders clientes
    $('#clientes-lista .lista, #clientes-lista-xs .lista').lightSlider({
        item: 1,
        slideMove: 1,
        loop: true
    });

    // formulario de acceso
    var $acceso = $('#acceso');
    $acceso.parent('.dropdown-menu').click(function (e) {
        e.stopPropagation();
    });
    $acceso.submit(function (e) {
        e.preventDefault();
        var data = {
            nombre: $acceso.find('#nombre').val().trim(),
            clave: $acceso.find('#clave').val()
        };
        var errorMsg = $acceso.find('p.text-danger');
        if (!data.nombre) {
            errorMsg.removeClass('text-hidden');
            errorMsg.text(errorMsg.data('errorNoNombre'));
            return;
        }
        if (!data.clave) {
            errorMsg.removeClass('text-hidden');
            errorMsg.text(errorMsg.data('errorNoClave'));
            return;
        }
        var button = $acceso.find('button');
        var url = $acceso.parent('.dropdown-menu').parent().find('a.dropdown-toggle').attr('href');
        $.ajax({
            type: $acceso.attr('method'),
            url: $acceso.attr('action'),
            dataType: 'json',
            contentType : 'application/json',
            data: JSON.stringify(data),
            success: function (data) {
                if (data.auth) {
                    window.location.href = url + '&auth=' + data.auth;
                } else {
                    errorMsg.removeClass('text-hidden');
                    errorMsg.text(errorMsg.data('errorInterno'));
                    setTimeout(function () {
                        window.location.href = url;
                    }, 400);
                }
            },
            error: function (respuesta) {
                errorMsg.removeClass('text-hidden');
                if (respuesta.status === 404) {
                    errorMsg.text(errorMsg.data('errorIncorrecto'));
                } else {
                    errorMsg.text(errorMsg.data('errorInterno'));
                    setTimeout(function () {
                        window.location.href = url;
                    }, 400);
                }
            },
            complete: function () {
                setTimeout(function () {
                    button.prop('disabled', false);
                }, 400);
            }
        });
        errorMsg.addClass('text-hidden');
        button.prop('disabled', true);
    });
});
