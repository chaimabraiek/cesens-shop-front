<?php

return [
    'executionParameters' => [
        'class' => Encore\Application\Controller\Parameters\SingleModuleLanguagePathParameters::class,
        'data'  => [
            'extension' => 'html',
        ],
    ],
    'autoloaders' => [
        'phpmailer' => [
            'path' => '/vendor/phpmailer',
            'namespace' => '\\PHPMailer\\PHPMailer',
        ],
    ],
    'filterStack' => [
        'session' => [
            'class'  => Encore\Cesens\Web\Filter\SessionFilter::class,
        ],
        'language' => [
            'class'  => Encore\Cesens\Web\Filter\LanguageFilter::class,
        ],
    ],
    'controllers' => [
        'default' => Encore\Cesens\Web\Controller\MainController::class,
    ],
    'i18n' => [
        'languages' => [
            'en',
            'es',
        ],
        'default' => 'en',
    ],
    'database' => include __DIR__ . '/private/database.php',
];
