<?php

namespace Encore\Cesens\Web\Filter;

use Encore\Application\Controller\Parameters\SingleModuleLanguagePathParameters;
use Encore\Application\Controller\Redirection;
use Encore\Application\Filter\AbstractFilter;
use Encore\Application\Filter\FilterStack;
use Encore\Application\View\View;
use Encore\Cesens\Web\Model\Language;
use Encore\Cesens\Web\Service\LanguageService;
use Encore\Config\Config;

class LanguageFilter extends AbstractFilter
{
    const DEFAULT_LANGUAGE = 'en';

    private $languageService;

    public function __construct(array $params = [])
    {
        parent::__construct($params);
        $this->languageService = new LanguageService();
    }

    public function filter(FilterStack $stack)
    {
        $language   = Language::fromString(self::DEFAULT_LANGUAGE);
        $ctrlParams = $stack->getApplication()->getControllerParameters();
        if ($ctrlParams instanceof SingleModuleLanguagePathParameters) {
            $language = $ctrlParams->getLanguage();
            if ($language === null) {
                $this->paramSetup($stack->getApplication()->getConfig());
                $available = $this->getParam('available');
                $default   = $this->getParam('default');
                $language  = $this->languageService->getClientLanguage($available, $default);
                $ctrlParams->setLanguage($language);
                return new Redirection($ctrlParams->getUrl(
                    $ctrlParams->getAction(),
                    $ctrlParams->getController(),
                    $ctrlParams->getModule(),
                    $_GET
                ));
            }
        }
        $_SESSION['cesens.com']['language'] = $language;
        $response = $stack->next();
        if ($response instanceof View) {
            $response->setVariable('language', $language);
        }
        return $response;
    }

    protected function paramSetup(Config $config)
    {
        $this->setParam('default', Language::fromString($config->get('i18n.default', self::DEFAULT_LANGUAGE)));
        $available = [];
        foreach ($config->get('i18n.languages', []) as $language) {
            $available[] = Language::fromString($language);
        }
        if (empty($available)) {
            $available[] = $this->getParam('default');
        }
        $this->setParam('available', $available);
    }
}
