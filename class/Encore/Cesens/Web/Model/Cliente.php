<?php

namespace Encore\Cesens\Web\Model;

class Cliente
{
    private $id;
    private $nombre;
    private $url;

    public function __construct($id, $nombre, $url = null)
    {
        $this->id = $id;
        $this->nombre = $nombre;
        $this->url = $url ?: null;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getNombre()
    {
        return $this->nombre;
    }

    public function getUrl()
    {
        return $this->url;
    }
}