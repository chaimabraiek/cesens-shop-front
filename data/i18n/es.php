<?php

return [
    'agricultura20' => 'Agricultura 4.0',
    'formato' => [
        'decimal' => ',',
        'miles'   => '.',
    ],
    'meta' => [
        'keywords' => 'estacion agroclimática, la rioja, españa, cesens, viñedo, evineyard, agricultura, precisión, información agroclimática, riego, cloud, encore lab',
        'description' => 'Cesens® es un sistema de información agroclimática para la toma de decisiones en agricultura. Conoce con precisión el estado de tus cultivos.',
    ],
    'menu' => [
        'video'      => 'Vídeo',
        'resumen'    => 'Resumen',
        'funciones'  => 'Funciones',
        'beneficios' => 'Beneficios',
        'datos'      => 'Datos',
        'clientes'   => 'Clientes',
        'contacto'   => 'Contacto',
        'app'        => 'Aplicación',
        'blog'       => 'Blog',
        'acceso'     => 'Acceder a la aplicación',
    ],
    'acceso' => [
        'usuario'   => 'Usuario o email',
        'clave'     => 'Contraseña',
        'acceder'   => 'Acceder',
        'recordar'  => 'Recordar contraseña',
        'registrar' => '¿No tienes una cuenta? Regístrate',
        'errores'   => [
            'no-nombre'  => 'Debes introducir un nombre o email',
            'no-clave'   => 'Debes introducir la clave',
            'incorrecto' => 'Los datos de acceso no válidos',
            'interno'    => 'Ha ocurrido un error interno',
        ],
    ],
    'video' => [
        'ver' => 'Ver vídeo',
        'banner' => [
            'texto'    => 'Ahora puedes utilizar Cesens<sup>®</sup> sin coste',
            'mas-info' => 'Más información',
        ],
    ],
    'resumen' => [
        'titulo'    => 'Bienvenido a la Agricultura 4.0',
        'subtitulo' => 'Conoce con precisión el estado de tus cultivos.',
        'intro' => [
            'titulo' => 'Cesens<sup>®</sup> es un sistema de información agroclimática para la toma de decisiones en agricultura.',
            'texto'  => 'Se basa en estaciones que recogen datos de los cultivos para obtener información de valor añadido, como el riesgo de infección o la necesidad de riego.',
        ],
        'estaciones' =>  [
            'titulo' => 'Estaciones',
            'texto'  => 'Las estaciones agroclimáticas Cesens<sup>®</sup> se instalan en las parcelas de cultivo y envían a la plataforma cloud los datos que recogen en tiempo real.',
            'img'    => 'Estación Cesens<sup>®</sup> en el campo',
        ],
        'cloud' =>  [
            'titulo' => 'Cloud',
            'texto'  => 'La plataforma cloud recibe los datos recogidos en el campo y los procesa, obteniendo información de gran valor para los técnicos agrícolas.',
            'img'    => 'Plataforma cloud Cesens<sup>®</sup>',
        ],
        'apps' =>  [
            'titulo' => 'Apps',
            'texto'  => 'Toda la información y funcionalidad de Cesens<sup>®</sup> es accesible desde cualquier tipo de dispositivo, en cualquier lugar del mundo.',
            'img'    => 'Cesens<sup>®</sup> en cualquier multidispositivo',
        ],
    ],
    'entrevistas' => [
        'titulo'    => '¿Sabías que…',
        'subtitulo' => '…contar con información agroclimática precisa es imprescindible para tomar las mejores decisiones sobre cualquier tipo de cultivo?',
        'texto'     => 'Cesens<sup>®</sup> proporciona a los técnicos agrícolas la capacidad de conocer el estado real de cada parcela cultivada, lo que les permite optimizar su gestión y reducir costes.',
    ],
    'estacion-viñedo' => [
        'titulo'    => 'Monitoriza cualquier tipo de cultivo',
    ],
    'funciones' => [
        'titulo'     => 'La agricultura del siglo XXI',
        'subtitulo'  => 'Una forma de agricultura más precisa y eficiente.',
        'titulo2'    => 'No te pierdas nada',
        'subtitulo2' => 'Cesens<sup>®</sup> vigila tus cultivos por ti.',
        'plagas' => [
            'titulo' => 'Plagas',
            'texto'  => 'Cesens<sup>®</sup> te permite conocer el riesgo de infección de plagas y enfermedades para que apliques los tratamientos sólo cuando sea necesario.',
            'img'    => 'Riesgo de infección',
        ],
        'riego' => [
            'titulo' => 'Riego',
            'texto'  => 'Cesens<sup>®</sup> monitoriza el estrés hídrico y/o contenido volumétrico de agua en suelo para que riegues tus parcelas en la medida y momento óptimo.',
            'img'    => 'Necesidad de riego',
        ],
        'seguimiento' => [
            'titulo' => 'Seguimiento',
            'texto'  => 'Con Cesens<sup>®</sup> puedes controlar la evolución de tus cultivos teniendo en cuenta cualquier variable agroclimática y estado fenológico.',
            'img'    => 'Evolución del cultivo',
        ],
        'historico' => [
            'titulo' => 'Histórico',
            'texto'  => 'Cesens<sup>®</sup> guarda toda la información de tus cultivos y te permite consultar datos históricos, incluso comparándolos con los de temporadas pasadas.',
            'img'    => 'Historial del cultivo',
        ],
        'alertas' => [
            'titulo' => 'Alertas',
            'intro'  => 'Sabemos que estás muy ocupado, por eso Cesens<sup>®</sup> te avisa cuando ocurre algo importante.',
            'texto'  => 'Cesens<sup>®</sup> cuenta con un potente sistema de alertas completamente configurable que te permite estar al tanto de lo que ocurre en tus cultivos en tiempo real y desde cualquier parte.',
            'img'    => 'Alertas personalizadas',
        ],
        'informes' => [
            'titulo' => 'Informes',
            'intro'  => 'Mantente al tanto de la evolución de tus cultivos de la forma más simple: desde tu correo electrónico.',
            'texto'  => 'Cesens<sup>®</sup> te permite configurar informes periódicos con la información que te parezca más relevante para el seguimiento de tus cultivos. Tan solo elige los datos que deseas recibir y la periodicidad.',
            'img'    => 'Informes a medida',
        ],
    ],
    'beneficios' => [
        'titulo'    => 'Grandes ventajas',
        'subtitulo' => 'Optimiza el uso de pesticidas y agua con Cesens<sup>®</sup>.',
        'calidad' => [
            'titulo' => 'Mejor calidad',
            'texto'  => 'Reducir el uso de pesticidas y regar en la medida y el momento precisos permite obtener cosechas más sanas y de mayor calidad.',
            'img'    => 'Obtén cosechas de mayor calidad',
        ],
        'costes' => [
            'titulo' => 'Menos costes',
            'texto'  => 'Regar y aplicar tratamientos en el momento oportuno en lugar de hacerlo con antelación supone un gran ahorro de costes de producción.',
            'img'    => 'Reduce costes de producción',
        ],
        'medioambiente' => [
            'titulo' => 'Cuidado ambiental',
            'texto'  => 'La gestión sostenible agua y pesticidas permite reducir la contaminación del medioambiente y conservar los recursos hídricos.',
            'img'    => 'Cuida más el medioambiente',
        ],
    ],
    'datos' => [
        'titulo'     => 'Visión en profundidad',
        'subtitulo' => 'Nunca antes has dispuesto de tanta información sobre tus cultivos.',
        'recogidos' => 'Datos recogidos',
        'tipos' => [
            'titulo' => 'Tipos de sensores',
            'texto'  => 'Cesens<sup>®</sup> soporta gran variedad de sensores de distintos fabricantes.',
            'lista' => [
                'titulo'  => '¡La lista es demasiado larga!',
                'texto'   => 'Disponemos de un catálogo de sensores en el que se detalla la relación de fabricantes, modelos, y otras características.',
                'masInfo' => 'Más información',
            ],
            'simultaneos' => [
                'titulo' => 'Sensores simultáneos',
                'texto'  => 'Cesens<sup>®</sup> permite conectar hasta %s sensores de forma simultánea.',
            ],
        ],
        'directas' => [
            'titulo' => 'Métricas directas',
            'texto'  => 'Son los datos que se recogen directamente desde los sensores.',
        ],
        'derivadas' => [
            'titulo' => 'Métricas derivadas',
            'texto'  => 'Se obtienen a partir de procesar los datos directos.',
        ],
        'modelos' => [
            'titulo' => 'Modelos predictivos',
            'texto'  => 'Muestran el índice de riesgo de infección en cada momento.',
        ],
        'mas' => [
            'titulo'    => '¿Quieres más?',
            'subtitulo' => 'Adaptamos la funcionalidad de Cesens<sup>®</sup> a tus necesidades.',
            'contacta'  => 'Contacta',
        ],
    ],
    'clientes' => [
        'titulo'    => 'Clientes',
        'subtitulo' => 'Algunos de los clientes que ya confían en Cesens<sup>®</sup>',
    ],
    'app' => [
        'titulo'    => 'Para nosotros, tu cultivo es lo primero',
        'subtitulo' => 'Ahora Cesens<sup>®</sup> no tiene coste',
        'texto'     => 'Como parte de nuestro compromiso con la evolución de la agricultura, ponemos a tu disposición de manera gratuita toda la funcionalidad de Cesens<sup>®</sup> para trabajar con datos procedentes de estaciones públicas. Entre otras cosas podrás:',
        'funciones' => [
            'alertas' => [
                'titulo' => 'Alertas',
                'texto'  => 'Recibir alertas: previsión de heladas, lluvias, viento…',
            ],
            'informes' => [
                'titulo' => 'Informes',
                'texto'  => 'Configurar informes automáticos',
            ],
            'prediccion' => [
                'titulo' => 'Predicción meteorológica',
                'texto'  => 'Consultar la predicción meteorológica',
            ],
            'graficas' => [
                'titulo' => 'Datos',
                'texto'  => 'Analizar datos mediante gráficas interactivas',
            ],
        ],
        'estacionesPublicas' => 'Redes de estaciones públicas',
        'registro' => [
            'titulo' => 'No pierdas la oportunidad',
            'web' => [
                'titulo' => 'Aplicación web',
                'texto'  => 'Accede desde tu navegador a todas las características y funcionalidad disponibles de Cesens<sup>®</sup>',
                'boton'  => 'Regístrate',
            ],
            'movil' => [
                'titulo' => 'App móvil',
                'texto'  => 'Ideal para consultar información climática y la predicción meteorológica de la forma más sencilla',
            ],
        ],
    ],
    'contacto' => [
        'titulo'    => 'Encantados de conocerte',
        'subtitulo' => 'Estamos a tu disposición para cualquier consulta.',
        'nombre'    => 'Nombre',
        'email'     => 'Email',
        'mensaje'   => 'Mensaje',
        'enviar'    => 'Enviar',
        'enviando'  => 'Enviando',
        'exito' => [
            'titulo' => 'Mensaje enviado',
            'texto'  => 'Gracias por ponerte en contacto con nosotros. En breve te responderemos.',
        ],
        'error' => [
            'titulo' => 'Error',
            'texto'  => 'No se ha podido enviar el mensaje. Por favor, inténtalo de nuevo más tarde.',
        ],
    ],
    'creditos' => [
        'productoDe' => 'es un producto de',
        'derechos'   => 'Todos los derechos reservados',
    ],
];
